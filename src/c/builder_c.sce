// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function builder_c()
    src_c_path = get_absolute_file_path("builder_c.sce");
    src_path = "c";
    linknames = "accsum";
    files = "accsum.c";
    ldflags = "";
    if getos() == "Windows" then
        // Configure the floating point unit so as to use
        // SSE2 units, potentially instead of x87 registers.
        cflags = "-DWIN32 -DACCSUM_EXPORTS /arch:SSE2";
        libs = [];
    else
        // Configure the floating point unit so as to use
        // SSE units, instead of registers.
        // The x86 registers may use extended precision.
        // http://gcc.gnu.org/wiki/FloatingPointMath
        include1 = src_c_path;
        cflags = "-mfpmath=sse -msse2 -I"""+include1+"""";
        libs = [];
    end

    tbx_build_src(linknames, files, src_path, src_c_path, "", ldflags, cflags);
endfunction

builder_c();
clear builder_c; // remove builder_c on stack
