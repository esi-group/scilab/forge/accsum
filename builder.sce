// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// builder.sce --
//   Builder for the Accsum Scilab Toolbox
//
mode(-1);
lines(0);

function accsumBuildToolbox()
    //setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES");
    v = getversion("scilab");
    if v(1) < 5 ..
     | (v(1) == 5 ..
      & v(2) <  3) then
        error(gettext("Scilab 5.3 or more is required."));
    end

    TOOLBOX_NAME = "accsum";
    TOOLBOX_TITLE = "Accsum";

    toolbox_dir = get_absolute_file_path("builder.sce");

    tbx_builder_src(toolbox_dir);
    tbx_builder_gateway(toolbox_dir);
    tbx_builder_macros(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

    clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
end

accsumBuildToolbox();
clear accsumBuildToolbox;
