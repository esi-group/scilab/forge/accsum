// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = accsum_shuffle(x)
    // Randomly shuffles the input.
    //
    // Calling Sequence
    //   y = accsum_shuffle(x)
    //
    // Parameters
    //   x : a m-by-n, matrix of doubles
    //   y : a m-by-n, matrix of doubles, the randomly permuted values.
    //
    // Description
    // Randomly shuffles the input.
    // This function makes use of the grand function and
    // modifies its state.
    //
    // Examples
    // // Use with larger r
    // x = accsum_wilkinson(10);
    // for i = 1 : 10
    //     x = accsum_shuffle(x);
    //     [s1,e1] = accsum_compsum(x);
    //     [s2,e2] = accsum_dblcompsum(x);
    //     mprintf("#%5d CS=%e + %e DCS=%e + %e\n",i,s1,e1,s2,e2);
    // end
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_shuffle", rhs, 1:1);
    apifun_checklhs("accsum_shuffle", lhs, 0:1);

    x = x(:);
    y = grand(1, "prm", x);
endfunction

