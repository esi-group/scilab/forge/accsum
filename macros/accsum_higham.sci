// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = accsum_higham()
    // Returns an example designed by Higham.
    //
    // Calling Sequence
    //   x = accsum_higham()
    //
    // Parameters
    //   x : a 4-by-1 matrix of doubles
    //
    // Description
    // This example is so that the orderings produce
    // different results.
    //
    // The exact sum is 1.
    //
    // Increasing magnitude and Psum (dynamic ordering, with order=1)
    // produce 0, while decreasing magnitude produce 1.
    //
    // Examples
    // x = accsum_higham()
    // sum(x)
    // // With direct input x: expected = 0
    // s = accsum_straight(x)
    // // With increasing magnitude ordering: expected = 0
    // s = accsum_straight(accsum_order ( x , 4 ))
    // // With dynamic magnitude ordering: expected = 0
    // s = accsum_orderdynamic ( x , 1 )
    // // With decreasing magnitude ordering: expected = 1
    // s = accsum_straight(accsum_order ( x , 5 ))
    //
    // Authors
    // Michael Baudin, 2010-2011
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al
    // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img16.htm

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_higham", rhs, 0:0);
    apifun_checklhs("accsum_higham", lhs, 0:1);

    M = 2 ^ 53;
    x = [1
         M
         2 * M
         -3 * M];
endfunction

