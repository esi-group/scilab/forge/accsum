// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s, e] = accsum_scs(x)
    // A Self Compensated Sum algorithm
    //
    // Calling Sequence
    //   s = accsum_scs(x)
    //   [s,e] = accsum_scs(x)
    //
    // Parameters
    //   x : a m-by-n, matrix of doubles
    //   s : a 1-by-1 matrix of doubles, the highest significant
    //       digits of the sum
    //   e : a 1-by-1, matrix of doubles, the lowest significant
    //       digits of the sum
    //
    // Description
    // A Self Compensated Sum algorithm.
    // Uses accsum_fasttwosum.
    //
    // Examples
    // [s,e] = accsum_scs ( [2 1] ) // 3
    // [s,e] = accsum_scs ( [1 2] ) // 3
    // [s,e] = accsum_DCS ( [2 1] ) // 3
    // [s,e] = accsum_DCS ( [1 2] ) // 3
    // x = accsum_wilkinson(10); size(x,"*")
    // s = sum(x)
    // [s,e] = accsum_scs ( x )
    // [s,e] = accsum_DCS ( x )
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al
    // https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img14.htm
    // https://hpcrd.lbl.gov/SCG/ocean/NRS/SCSsum.F

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_scs", rhs, 1:1);
    apifun_checklhs("accsum_scs", lhs, 0:2);

    s = 0;
    e = 0;
    n = size(x, "*");
    for i = 1 : n
        [s, e] = accsum_fasttwosum(s, x(i) + e);
    end
endfunction

