// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function s = accsum_straight(x)
    // The straightforward sum of a matrix.
    //
    // Calling Sequence
    //   s = accsum_straight(x)
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   s : a 1-by-1, matrix of doubles, the sum
    //
    // Description
    // Computes the sum by a straightforward sum.
    // This is called "recursive summation" in SNAA.
    //
    // Uses an unvectorized loop.
    // This is a slow algorithm, used for comparison purposes
    // only.
    //
    // Caution!
    // This function may not return the same
    // value as the "sum" function of Scilab!
    // This is because the Intel MKL or ATLAS may
    // reorder the data, processing it block by block
    //
    // Examples
    //  x = 1:10;
    //  accsum_straight(x)
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_straight", rhs, 1:1);
    apifun_checklhs("accsum_straight", lhs, 0:1);
      //
    n = size(x, "*");
    s = 0;
    for i = 1 : n
        s = s + x(i);
    end
endfunction

