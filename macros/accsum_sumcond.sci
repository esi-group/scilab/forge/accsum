// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s, c] = accsum_sumcond(x)
    // Condition number of the sum function.
    //
    // Calling Sequence
    //   s = accsum_sumcond(x)
    //   [s,c] = accsum_sumcond(x)
    //
    // Parameters
    //   x : a m-by-n, matrix of doubles
    //   s : a 1-by-1, matrix of doubles, the sum
    //   c : a 1-by-1, matrix of doubles, the condition number
    //
    // Description
    // Condition number of the sum function.
    //
    // Examples
    // // A straightforward example of an ill-conditionned data for the sum.
    // xl = 10 ^ (1:15);
    // x = [-xl xl+0.1];
    // sum(x)
    // [s,c] = accsum_sumcond(x)
    // for o = 1 : 8
    //     xo = accsum_order(x, o);
    //     [s, e] = accsum_dcs(xo);
    //     disp([o s e]);
    // end
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_sumcond", rhs, 1:1);
    apifun_checklhs("accsum_sumcond", lhs, 0:2);

    s = sum(x);
    varf = sum(abs(x));
    c = varf / abs(s);
endfunction

