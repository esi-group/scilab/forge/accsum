// Copyright (C) 2010 - 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s, e] = accsum_compsum(x)
    // The compensated sum of a matrix.
    //
    // Calling Sequence
    //   s = accsum_compsum ( x )
    //   [s,e] = accsum_compsum ( x )
    //
    // Parameters
    //   x : a n-by-m matrix of doubles
    //   s : a 1-by-1 matrix of doubles, the highest significant
    //       digits of the sum
    //   e : a 1-by-1, matrix of doubles, the lowest significant
    //       digits of the sum
    //
    // Description
    // Returns s and e such that s+e is the sum of x.
    // Algorithm 4.4 in HFCC
    // Due to Knuth
    // No assumption on a , b
    // Assumes that Scilab uses round-to-nearest.
    //
    // Examples
    // [s,e] = accsum_compsum ( [2 1] ) // 3
    // [s,e] = accsum_compsum ( [1 2] ) // 3
    // // is 1+(%eps/2) but is 1 without algo
    // [s,e] = accsum_compsum ( [1 %eps/2 ] )
    // // is 1+(%eps/2) but is 1 without algo
    // [s,e] = accsum_compsum ( [%eps/2 1] )
    //
    // Authors
    // Michael Baudin, 2010
    //
    // Bibliography
    // "Stability and numerical accuracy of algorithms", Nicolas Higham
    // "Handbook of Floating Point Computations", Muller et al

    [lhs, rhs] = argn();
    apifun_checkrhs("accsum_compsum", rhs, 1:1);
    apifun_checklhs("accsum_compsum", lhs, 0:2);

    s = 0;
    e = 0;
    n = size(x, "*");
    for i = 1 : n
        t = s;
        y = x(i) + e;
        s = t + y;
        e = (t - s) + y;
    end
endfunction

