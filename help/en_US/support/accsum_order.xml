<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from accsum_order.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="accsum_order" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>accsum_order</refname><refpurpose>Re-order the matrix.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   y = accsum_order ( x , o )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a m-by-n, matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>o :</term>
      <listitem><para> a 1-by-1 matrix of doubles, a positive integer, the order. o = 1: no-op i.e. y=x, o = 2: increasing order, o = 3: decreasing order, o = 4: increasing magnitude order, o = 5: decreasing magnitude order (good order for DCS), o = 6: positive reverse from order 2, o = 7: negative reverse from order 2, o = 8: random order</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a m-by-n, matrix of doubles, the same content as x, but in different order.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Re-order the matrix.
If o=8, then this function uses the grand function and
modifies its state.
   </para>
   <para>
The increasing magnitude order (o=4) is good when summing nonnegative
numbers by recursive summation (i.e. with accsum_straight),
in the sense of having the smallest a priori forward error bound.
   </para>
   <para>
The decreasing magnitude ordering order (o=5) is good when
there is heavy cancellation, i.e. when the condition number
returned by accsum_sumcond is large.
In this case, the recursive summation (i.e. accsum_straight)
can be used with decreasing magnitude ordering.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
x = [-9.9 5.5 2.2 -3.3 -6.6 0 1.1]
for o = 1 : 8
y = accsum_order ( x , o );
disp([o y'])
end

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, 2010</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Stability and numerical accuracy of algorithms", Nicolas Higham</para>
   <para>"Handbook of Floating Point Computations", Muller et al</para>
   <para>https://hpcrd.lbl.gov/SCG/ocean/NRS/ECMWF/img16.htm</para>
</refsection>
</refentry>
