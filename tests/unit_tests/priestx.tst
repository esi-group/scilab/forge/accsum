// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

x = accsum_priestx ( )
expected = [
  18014398509481984;
  18014398509481982;
  -9007199254740991;
  -9007199254740991;
  -9007199254740991;
  -9007199254740991
];
assert_checkequal(x,expected);

