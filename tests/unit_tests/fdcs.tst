// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[s,e] = accsum_fdcs ( [2 1] );
assert_checkequal(s,3);
assert_checkequal(e,0);
//
[s,e] = accsum_fdcs ( [1 2] );
assert_checkequal(s,3);
assert_checkequal(e,0);
//
[s,e] = accsum_fdcs ( [2 1] );
assert_checkequal(s,3);
assert_checkequal(e,0);
//
[s,e] = accsum_fdcs ( [1 2] );
assert_checkequal(s,3);
assert_checkequal(e,0);
//
x = accsum_wilkinson(10);
[s,e] = accsum_fdcs ( x );
assert_checkalmostequal(s,1024,1.e-12);
assert_checkalmostequal(e,-3.786e-14,[],1.e-10);

