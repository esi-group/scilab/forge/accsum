// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Test on simple data
x = [-9.9 5.5 2.2 -3.3 -6.6 0 1.1]
s = accsum_orderdynamic ( x , 1 );
assert_checkequal(s,-11);
s = accsum_orderdynamic ( x , 2 );
assert_checkequal(s,-11);


