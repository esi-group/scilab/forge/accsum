// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Example with r = 3
x = accsum_wilkinson (3);
// Expected result
e = [1 , 1-%eps , 1-2*%eps , 1-2*%eps , 1-4*%eps , 1-4*%eps , 1-4*%eps , 1-4*%eps]';
assert_checkalmostequal(x,e,3*%eps);


