// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[s,t] = accsum_fasttwosum ( 2 , 1 );
assert_checkequal(s,3);
assert_checkequal(t,0);
//
// is 1+(%eps/2) but is 1 without algo
[s,t] = accsum_fasttwosum ( 1 , %eps/2 );
assert_checkequal(s,1);
assert_checkequal(t,%eps/2);

