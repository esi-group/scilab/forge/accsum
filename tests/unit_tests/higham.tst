// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

x = accsum_higham ( );
// With direct input x: expected = 0
s = accsum_straight(x);
assert_checkequal(s,0);
// With increasing magnitude ordering: expected = 0
s = accsum_straight(accsum_order ( x , 4 ));
// With dynamic magnitude ordering: expected = 0
assert_checkequal(s,0);
s = accsum_orderdynamic ( x , 1 );
assert_checkequal(s,0);
// With decreasing magnitude ordering: expected = 1
s = accsum_straight(accsum_order ( x , 5 ));
assert_checkequal(s,1);
