// Copyright (C) 2010-2011 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include "machine.h"
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"

#include "accsum.h"

// s = accsum_fcompsum(x)
// [s,e] = accsum_fcompsum(x)
// returns the sum of x, with a compensated summation.
#if SCI_VERSION_MAJOR < 6
int sci_accsum_fcompsum(char * fname)
#else
int sci_accsum_fcompsum(char * fname, void * pvApiCtx)
#endif
{

    SciErr sciErr;
    int * piAddr = NULL;
    int iType = 0;
    int rowsX;
    int colsX;
    double * lX = NULL;
    int iComplex = 0;
    int i;
    double s;
    double e;

    int minlhs = 1;
    int minrhs = 1;
    int maxlhs = 2;
    int maxrhs = 1;

    CheckRhs(minrhs, maxrhs) ;
    CheckLhs(minlhs, maxlhs) ;

    //
    // Read X
    //
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    sciErr = getVarType(pvApiCtx, piAddr, &iType);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if ( iType != sci_matrix )
    {
        Scierror(999, _("%s: Wrong type for input argument #%d. "
                        "Matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    iComplex    = isVarComplex(pvApiCtx, piAddr);
    if ( iComplex == 1 )
    {
        Scierror(999, _("%s: Wrong content for input argument #%d. "
                        "Real matrix expected.\n"),
                 fname, 1);
        return 1;
    }
    sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &rowsX, &colsX, &lX);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    //
    // Perform the sum
    //
        accsum_fcompsum(lX, rowsX*colsX, &s, &e);
        //
        // Set LHS
        if ( Lhs>=1 )
        {
            //
            // Create s
            sciErr = createMatrixOfDouble(pvApiCtx,Rhs+1, 1, 1, &s);
            if(sciErr.iErr)
            {
                printError(&sciErr, 0);
                return 1;
            }
            LhsVar(1) = Rhs+1;
        }
        if ( Lhs==2 )
        {
            //
            // Create e
            sciErr = createMatrixOfDouble(pvApiCtx,Rhs+2, 1, 1, &e);
            if(sciErr.iErr)
            {
                printError(&sciErr, 0);
                return 1;
            }
            LhsVar(2) = Rhs+2;
        }
    return 0;
}

